<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'R1Y6p0?!150' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R%_p~28C^cB]NshN_6:>QrbT([Pa-KmOPyS|+5MBM6{d7t|;{*G$U$&[,_z#YH;`' );
define( 'SECURE_AUTH_KEY',  ';I|LgWdbnJ o=~J4iu&0Z!l}ZY8~sEy9J~Mb-+R;zMja*vY!S_!]i@?kb3/6}oPi' );
define( 'LOGGED_IN_KEY',    '-KMzQ3=&@$vafK1]5^nM@pi75<1^4}1BPIBGqH*US.U`&}JX{@Z9[Ixh`_>;-::P' );
define( 'NONCE_KEY',        '*@pRRD#x>]?jZ@G2_k/-AP>{zs*1+>A-SSb,`Q[:,$7WX!^rO|k8-tuS:,s7)7F6' );
define( 'AUTH_SALT',        'MD{j[NB*`bJCV5?5{-V.v{++&24/mSXI5o+5qlPe;[p^HJNTzP4LMy+BM(emb=FA' );
define( 'SECURE_AUTH_SALT', '&pXVwfJ&W)5S(Kqwd{-crbPp<9_6tUl7cIr<~N:OL58&h5Ee|+[4lR7t}Vd{)1S}' );
define( 'LOGGED_IN_SALT',   'gRUZZ&nXle9k0~]uR]v#*gP;Xk_*Z6oEu9s}UzMBIKhH_fo%^p(r4Y7;8+2#~R1G' );
define( 'NONCE_SALT',       '$Yb3=F0=K6g:[hUj9%F2L2sp0R$LK>o/k(|pD2k.I:ixYwN{vGEj;Y+^xv_[~oE[' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

